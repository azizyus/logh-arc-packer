//
// Created by d on 29-10-19.
//

#include "file-meta.h"
#include <list>
#include <iterator>

#ifndef LOGH_ARC_PACKER__FILE_PARSER_H
#define LOGH_ARC_PACKER__FILE_PARSER_H
class FileParser
{

    HeaderParser *headerParser;
    int fileMetaLength = 32; // this 32 byte meta data per file


public:
    list <FileMeta>::iterator fit;
    list <FileMeta> fileMetaList;
    unsigned char *archiveBytesPointer;

    void setHeaderParser(HeaderParser headerParser)
    {
        this->headerParser = &headerParser;
        this->parseFileMeta();
    }

    void setArchiveBytes(unsigned char *archiveBytes)
    {
        this->archiveBytesPointer = archiveBytes;
    }

    void parseFileMeta()
    {

        printf("[FileParser] File count is %d \n",this->headerParser->fileCount);
        for (int i = 0; i < this->headerParser->fileCount; i++)
        {
            int startAddress = this->headerParser->metaStartAddress + (this->fileMetaLength * i);
            printf("start Address is : %2x\n",startAddress);
            FileMeta newFileMeta;

            newFileMeta.fileNameStartOffset = this->archiveBytesPointer[(startAddress + 0x2)];

            newFileMeta.fileNameStartAddress  = newFileMeta.fileNameStartOffset + ((this->archiveBytesPointer[(startAddress + 0x09)] << 8) | this->archiveBytesPointer[(startAddress + 0x0A)]);
            newFileMeta.fileNameLength = this->archiveBytesPointer[(startAddress+0x3)];

            newFileMeta.fileName = this->getFileNameFromBytes(
                    newFileMeta.fileNameStartAddress,
                    newFileMeta.fileNameLength
                    );

            newFileMeta.compressedDataSize =
                    (this->archiveBytesPointer[(startAddress + 0x14)]  << 16) |
                    (this->archiveBytesPointer[(startAddress + 0xD)]  << 8) |
                    (this->archiveBytesPointer[(startAddress + 0x06)]);

            newFileMeta.fileDataStartAddress =
                             (this->archiveBytesPointer[(startAddress + 0x19)] << 24) |
                             (this->archiveBytesPointer[(startAddress + 0x12)] << 16) |
                             (this->archiveBytesPointer[(startAddress + 0xB)] << 8) |
                             (this->archiveBytesPointer[(startAddress + 0x4)]);


            newFileMeta.fileDataSize =
                    (this->archiveBytesPointer[(startAddress + 0x13)] << 16) |
                    (this->archiveBytesPointer[(startAddress + 0x0C)] << 8) |
                    (this->archiveBytesPointer[(startAddress + 0x05)]);
            this->fileMetaList.push_back(newFileMeta);
        }
        printf("---------------------------------------\n");

        for(fit = this->fileMetaList.begin(); fit!=this->fileMetaList.end(); fit++)
        {
            cout << "File NAME: " << fit->fileName << "\n";
            printf("File NAME start address: 0x%2x \n", fit->fileNameStartAddress);
            printf("File NAME length: %d character (byte) \n", fit->fileNameLength);
            printf("<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>\n");

            printf("File DATA start Address: 0x%4x \n", fit->fileDataStartAddress);
            printf("File DATA size: %d byte \n", fit->fileDataSize);
            printf("File Data compressed size: 0x%04x byte \n",fit->compressedDataSize);
            //printf("File name: %s \n", fit->fileName);
            printf("---------------------------------------\n");

        }

    }

private:
    string getFileNameFromBytes(int start,int length)
    {
        string temp;
        int startAddress = start;
        int endAddress = (startAddress + length);
        for (int i = startAddress; i < endAddress; ++i)
        {
                temp+=this->archiveBytesPointer[i];
        }
        return temp;
    }

};

#endif //LOGH_ARC_PACKER__FILE_PARSER_H
