//
// Created by d on 29-10-19.
//

#include <string>
#include "file-parser.h"
#include <fstream>
#include "fileapi.h"
using namespace std;

#ifndef LOGH_ARC_PACKER__FILE_EXTRACTOR_H
#define LOGH_ARC_PACKER__FILE_EXTRACTOR_H


class FileExtractor
{


public:
    string extractDirectory;

    void setExtractDirectory(string directory)
    {
        this->extractDirectory = directory;
    }

    void extractFilesToDirectory(FileParser *fileParser)
    {
        for(fileParser->fit = fileParser->fileMetaList.begin(); fileParser->fit!=fileParser->fileMetaList.end(); fileParser->fit++)
        {
//            unsigned char temp[fileParser->fit->fileDataSize];
            unsigned char *temp;
            int tempSize = fileParser->fit->fileDataSize;
            temp = (unsigned char*)malloc(tempSize);
//            return;
            (fileParser->fit->getFileData(temp,fileParser->archiveBytesPointer));
            this->extractDataToDirectory(
                    fileParser->fit->fileName,
                    fileParser->fit->fileDataSize,
                    temp
                    );
            delete(temp);
        }
    }

    void extractDataToDirectory(string fileName,int size, unsigned char *bytes)
    {
        string fullPath = this->extractDirectory+"/"+fileName;
        std::ofstream file;
        CreateDirectoryA(GetDirectory(fullPath).c_str(), NULL );
        file.open(fullPath, fstream::binary | fstream::out);
        file.write((const char *)(bytes), size);
        file.close();
        cout << "!OK " << (fullPath) << endl;

    }
    string GetDirectory (const std::string& path)
    {
        size_t found = path.find_last_of("/\\");
        return(path.substr(0, found));
    }

};

#endif //LOGH_ARC_PACKER__FILE_EXTRACTOR_H
