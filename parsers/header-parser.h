//
// Created by dd on 29.10.2019.
//

#ifndef LOGH_ARC_PACKER__HEADER_PARSER_H
#define LOGH_ARC_PACKER__HEADER_PARSER_H

#include <string>

using namespace std;

class HeaderParser
{

private:


public:
    unsigned char headerBytes[128];
    unsigned char archiveType[4];
    unsigned char headFileCountAddress = 0XD;
    unsigned char headMetaStartAddress = 0x4;
    unsigned char headCompressionAddress = 0xF;
    unsigned char metaStartAddress;
    unsigned char compressionType;
    int nameDataStartAddress;
    int fileCount;


    HeaderParser()
    {

    }

    void setHeaderBytes(unsigned char fileBytes[])
    {

        //printf("example data : %c%c%c%c \n",fileBytes[0],fileBytes[1],fileBytes[2],fileBytes[3]);

        for (int i = 0; i < 128; ++i)
        {
            this->headerBytes[i] = (fileBytes[i]); //gather header
        }


        this->parseArchiveTypeBytes();
        this->parseMetaStartAddress();

        this->parseNameDataStartAddress();
        this->parseFileCount();
    }

    void setFileCountByte(unsigned char fileCount)
    {
        this->headerBytes[this->headFileCountAddress] = fileCount;
    }

    void setArchType(unsigned char *bytes)
    {
        this->headerBytes[0] = (bytes[0]);
        this->headerBytes[1] = (bytes[1]);
        this->headerBytes[2] = (bytes[2]);
        this->headerBytes[3] = (bytes[3]);
    }

    void setFileSize(int size)
    {
        this->headerBytes[0x10] = size & 0xFF; //file size suffix
        this->headerBytes[0x11] = (size >> 8) & 0xFF; //filesize middle
        this->headerBytes[0x23] = (size >> 16) & 0xFF; //file size prefix
    }

    void setCompressionType(unsigned char type)
    {
        this->headerBytes[this->headCompressionAddress] = type;
    }

    void parseCompressionType()
    {
        this->compressionType = this->headerBytes[this->headCompressionAddress];
        printf("Compression type is 0x%02x \n",this->compressionType);
    }

    void parseFileCount()
    {
        this->fileCount = this->headerBytes[this->headFileCountAddress];
        printf("[Header Parser] File count is: %d \n",this->fileCount);
    }

    void parseNameDataStartAddress()
    {
        this->nameDataStartAddress = ((this->headerBytes[0x12] << 8) | (this->headerBytes[0x13])) + this->headerBytes[0x8];
        printf("Name data start address : %4x \n",this->nameDataStartAddress);
    }

    void parseMetaStartAddress()
    {
        this->metaStartAddress = this->headerBytes[this->headMetaStartAddress];
        printf("Meta start address : %2x \n",this->metaStartAddress);
    }

    void parseArchiveTypeBytes()
    {
        this->archiveType[0] = (this->headerBytes[0x0]);
        this->archiveType[1] = (this->headerBytes[0x1]);
        this->archiveType[2] = (this->headerBytes[0x2]);
        this->archiveType[3] = (this->headerBytes[0x3]);

        printf("Archive type : %c%c%c%c \n",this->archiveType[0],this->archiveType[1],this->archiveType[2],this->archiveType[3]);

    }


};

#endif //LOGH_ARC_PACKER__HEADER_PARSER_H
