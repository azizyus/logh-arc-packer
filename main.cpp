#include <iostream>
#include <string>
#include "arc-parser.h"

#include "parsers/header-parser.h"

using namespace std;

int main(int argc, char** argv)
{

    ArcParser *arcParser = new ArcParser();
    arcParser->setArchiveFileName("../test/loading_img.arc");
    arcParser->setOutputDirectoryPath("../output_test");
    arcParser->init();
    arcParser->extractFiles();

    printf("\nDONE");

    return 0;
}