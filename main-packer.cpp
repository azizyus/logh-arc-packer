#include <iostream>
#include <string>

#include "arc-packer.h"

using namespace std;

int main(int argc, char** argv)
{
    cout << "unpacker" << endl;

    ArcPacker *arcPacker = new ArcPacker();
    arcPacker->setRootDirectory("../output_test");
    arcPacker->setExportFileName("../packed_test/loading_img_test.arc");
    arcPacker->init();
    arcPacker->printFileList();
    arcPacker->packFiles();
    return 0;
}