//
// Created by d on 30-10-19.
//
#include <string>
#include "packers/file-reader.h"
#include <dirent.h>
#include <iterator>
#include <sys/types.h>
#include <vector>
#include "parsers/header-parser.h"
#include <fstream>
#include "parsers/file-meta.h"

#ifndef LOGH_ARC_PACKER__ARCK_PACKER_H
#define LOGH_ARC_PACKER__ARCK_PACKER_H

class ArcPacker
{

public:

    string rootDirectory;
    string exportFileName;
    FileReader *fileReader;
    unsigned char compressionStatus;
    char fileNameSeparator=' ';

    void withCompression()
    {
        this->compressionStatus = 0x03;
        this->fileNameSeparator = ' ';
    }

    void withoutCompression()
    {
        this->compressionStatus = 0x00;
        this->fileNameSeparator = '\0';
    }

    void init()
    {
        this->fileReader = new FileReader(this->rootDirectory);
    }

    void setRootDirectory(string directory)
    {
        this->rootDirectory = directory;
    }

    void setExportFileName(string fileName)
    {
        this->exportFileName = fileName;
    }


    void printFileList()
    {
        printf("file list: \n");

        vector<string> v;

        this->fileReader->getFileList(&v);
        for(auto t=v.begin(); t!=v.end(); ++t)
        {
            cout << "FILE: " << *t << '\n';
        }


        printf("OK");


    }


    void packFiles()
    {

        vector<string> v;
        this->fileReader->getFileList(&v);


        unsigned char * packBytes;
        packBytes = (unsigned char* )malloc(1024*1024*50);

        for (int j = 0; j < sizeof(packBytes); ++j) {
            packBytes[j] = 0x00; //lets be sure we have bunch pure 0x00
        }
        HeaderParser *header = new HeaderParser();
        for (int j = 0; j < sizeof(header->headerBytes); ++j) {
            header->headerBytes[j] = 0x00; //lets be sure we have bunch pure 0x00
        }



        header->setFileCountByte(v.size());
        header->parseFileCount();

        unsigned char archType[] = {0x41,0x52,0x43,0x31};
        header->setArchType(archType);
        header->parseArchiveTypeBytes();

        header->setCompressionType(this->compressionStatus);
        header->parseCompressionType();

        header->headerBytes[header->headMetaStartAddress] = 0x80;
        header->headerBytes[0x5] = 0x20;
        header->headerBytes[0x6] = 0x0;
        header->headerBytes[0x7] = 0x0;
        header->headerBytes[0x8] = 0x0;

        //printf("file count is",header->nextFileMetaByteStartAddressOffset);

        vector<FileMeta> fileMetas;


        int fileCounter = 0;
        int lastFileNameSize = 0;
        int lastFileNameOffset = 0;


        //32 is double line space
        int totalSpentBytes = 128 + (32 * header->fileCount) + 32;
        int offsetForFileNames = totalSpentBytes;
        int totalFileNameSize = 0;
//        printf("total bytes %d",totalSpentBytes);
//        exit(0);
        for(auto t=v.begin(); t!=v.end(); ++t)
        {


            string fakeFileName = (*t)+this->fileNameSeparator;
//            cout << "FILE: " << *t << '\n';

            FileMeta *newFileMeta = new FileMeta();

            newFileMeta->fileName = fakeFileName;
            newFileMeta->fileNameLength = fakeFileName.size();

            if(fileCounter == 0)
            {
                newFileMeta->fileNameStartOffset =  0x00;
            }
            else if(totalFileNameSize >= 256)
            {

                short nextOverflowOffset = (totalFileNameSize - 256);
                offsetForFileNames += totalFileNameSize;

                totalFileNameSize = 0;
//                newFileMeta->fileNameStartOffset = lastFileNameSize + lastFileNameOffset;
                newFileMeta->fileNameStartOffset = lastFileNameSize + lastFileNameOffset - nextOverflowOffset;

            }
            else
            {
                // bin/FB_IMG_1467540129159.jpg
                newFileMeta->fileNameStartOffset = lastFileNameSize + lastFileNameOffset;
            }

            lastFileNameSize = newFileMeta->fileNameLength;
            lastFileNameOffset = newFileMeta->fileNameStartOffset;
            newFileMeta->fileStartOffsetPrefix = offsetForFileNames;


            ifstream file( this->rootDirectory+"/"+*t, ios::binary | ios::ate);
            newFileMeta->fileDataSize = file.tellg();
            fileMetas.push_back(*newFileMeta);
            printf("file name start address 0x%04x \n",(lastFileNameOffset+lastFileNameSize));
//            printf("file name start offsets 0x%04x \n",lastFileNameOffset);
//            printf("file size %d \n",newFileMeta->fileDataSize);
            fileCounter++;
            totalFileNameSize += lastFileNameSize;
        }


        //WRITING TO BUFFER
        //header bytes
        for (int i = 0; i < 128; ++i) {
            packBytes[i] = header->headerBytes[i];
        }
        int bufferPointer = 128;



        //file meta bytes
        int nextFileMetaByteStartAddressOffset = 0;
        int lastFileDataSize = 0;
        int lastFileDataAddress = 0;

        int fileMetaBufferPointer = bufferPointer;
        for(auto f=fileMetas.begin(); f!=fileMetas.end(); ++f)
        {
            f->fileStartOffsetPrefix += 64;

            int packMetaOffsetAddress = bufferPointer + (nextFileMetaByteStartAddressOffset);
            packBytes[packMetaOffsetAddress+0x2] =  f->fileStartOffsetPrefix + f->fileNameStartOffset;
            packBytes[packMetaOffsetAddress+0x3] = f->fileNameLength;
            packBytes[packMetaOffsetAddress+0x4] = f->fileDataStartAddress & 0x0000FF;


            //data size
            int fileDataSize = f->fileDataSize;
            packBytes[packMetaOffsetAddress+0x13] =  (fileDataSize >> 16);
            packBytes[packMetaOffsetAddress+0xC] =  (fileDataSize >> 8);
            packBytes[packMetaOffsetAddress+0x5] =  (fileDataSize & 0x00FF);


            //compressed data size
            packBytes[packMetaOffsetAddress+0xD] =  (fileDataSize >> 8);
            packBytes[packMetaOffsetAddress+0x6] =  (fileDataSize & 0x00FF);

            packBytes[packMetaOffsetAddress+0x9] = (f->fileStartOffsetPrefix >> 8); //file name start address part1
            packBytes[packMetaOffsetAddress+0xA] = 0x00; //file name start address part2



            nextFileMetaByteStartAddressOffset+=32;

        }
        bufferPointer += fileCounter * 32;


        //file name start address
        bufferPointer += 96; //name listing address
//        header->headerBytes[0x09] = 0x3D; //saber

        header->headerBytes[0x09] = 0xF7;
        header->headerBytes[0x0A] = 0x80;
        header->headerBytes[0x0C] = 0x02;
        header->headerBytes[0x11] = 0x02; //filesize middle
        header->headerBytes[0x12] = (bufferPointer >> 8);
        header->headerBytes[0x13] = (bufferPointer  & 0x00FF);

//        header->headerBytes[0x14] = 0x03; //saber
//        header->headerBytes[0x15] = 0x01; //saber
        header->headerBytes[0x14] = 0x02;
        header->headerBytes[0x15] = 0x00;
        header->headerBytes[0x16] = 0x04;
        header->headerBytes[0x17] = 0xD8;
        header->headerBytes[0x23] = 0x06; //file size prefix

        // i believe there is something related to filesize or something like that
        header->setFileSize(5902336);

        for (int i = 0; i < 128; ++i) {
            packBytes[i] = header->headerBytes[i];
        }

        //write file names to buffer
        for(auto t=v.begin(); t!=v.end(); ++t)
        {
            string tempFileName = *t+this->fileNameSeparator;
            int tempFileNameSize = tempFileName.size();
            for (int i = 0; i < tempFileNameSize; ++i)
            {
                packBytes[bufferPointer+i] = tempFileName[i];
            }
            bufferPointer+=tempFileNameSize;
        }

        bufferPointer += 249+16; //space for data





        nextFileMetaByteStartAddressOffset = 0;
        int bufferedFileSize = 0;
        int fileCount = 0;
        for(auto f=fileMetas.begin(); f!=fileMetas.end(); ++f)
        {
            int packMetaOffsetAddress = fileMetaBufferPointer + (nextFileMetaByteStartAddressOffset);


            int fileDataStartAddress = bufferPointer + bufferedFileSize;

            int tempAddress = fileDataStartAddress & 0xFF;

//            if(tempAddress < 0x80)
//            {
//                fileDataStartAddress += (0x80-tempAddress);
//            }
//            else
//            {
//                fileDataStartAddress -= (tempAddress - 0x80);
//
//            }
////            fileDataStartAddress = tempAddress;

//            fileDataStartAddress = (fileDataStartAddress - (fileDataStartAddress % 16));

            packBytes[packMetaOffsetAddress+0xB] = fileDataStartAddress >> 8;
            packBytes[packMetaOffsetAddress+0x4] = fileDataStartAddress & 0x0000FF;
            int fileDataStartAddressPart2 =   packBytes[packMetaOffsetAddress+0x4];


            if(fileDataStartAddress > 0xFFFF) //when you overflow the 0xFFFF you need to store overflowed part to specified address
            {
                packBytes[packMetaOffsetAddress+0x12] = fileDataStartAddress >> 16;
            }

            if(fileDataStartAddress > 0xFFFFFF) //when you overflow the 0xFFFF you need to store overflowed part to specified address
            {
                packBytes[packMetaOffsetAddress+0x19] = fileDataStartAddress >> 24;
            }

            printf("file data start address 0x%8x \n",fileDataStartAddress);
            ifstream file( "../output_test/"+f->fileName, ifstream::binary | ifstream::out);
            char * fileBuffer;
            fileBuffer = (char * )malloc(f->fileDataSize);
            file.read(fileBuffer, f->fileDataSize);
//

//             printf("data start address is %04x \n",fileDataStartAddress);

//             printf("size is %02x \n",f->fileDataSize);

            for (int i = 0; i < f->fileDataSize; ++i)
            {
//                printf("data: %2x \n", fileBuffer[i]);
//                printf("write address: %4x \n", fileDataStartAddress);
                packBytes[fileDataStartAddress+i] = fileBuffer[i];
            }
            nextFileMetaByteStartAddressOffset+=32;





            //compressed data size
//            packBytes[0x80+(32*fileCount)+0x6] =  (fileBuffer[f->fileDataSize-1]);
//            packBytes[0x80+(32*fileCount)+0xD] =  (fileBuffer[f->fileDataSize-2]);
//            packBytes[0x80+(32*fileCount)+0x14] =  (fileBuffer[f->fileDataSize-3]);



//            printf("----------------------------------");


            if(f->fileDataSize % 2048 != 0)
            {
                f->fileDataSize += (2048 - (f->fileDataSize % 2048));
            }

            bufferedFileSize += f->fileDataSize; //16 line space between data

            fileCount++;
        }
        bufferPointer += bufferedFileSize;















        string fullPath = this->exportFileName;
        std::ofstream file;
        file.open(fullPath, fstream::binary | fstream::out);
        for (int i = 0; i < 128; ++i) {
            packBytes[i] = header->headerBytes[i];
        }
        file.write((const char *)packBytes, bufferPointer);
        file.close();







    }



};

#endif //LOGH_ARC_PACKER__ARCK_PACKER_H
