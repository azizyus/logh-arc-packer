//
// Created by d on 30-10-19.
//

#include <string>
#include <dirent.h>
#include <iterator>
#include <sys/types.h>
#include <vector>

#ifndef LOGH_ARC_PACKER__FILE_READER_H
#define LOGH_ARC_PACKER__FILE_READER_H

using namespace std;

class FileReader
{

public:

    string rootDirectory;
    FileReader(string rootDirectory = "./")
    {
        this->rootDirectory = rootDirectory;
    }

    void getFileList( vector<string> *v)
    {
        DIR* dirp = opendir(this->rootDirectory.c_str());
        struct dirent * dp;
        while ((dp = readdir(dirp)) != NULL) {
           if(strcmp(dp->d_name,".gitkeep") && strcmp(dp->d_name,"..") && strcmp(dp->d_name,".")  && strcmp(dp->d_name,"my.datatable.arc"))
           {

               if(strcmp(dp->d_name,"bin")==0)
               {
                   FileReader *f = new FileReader(this->rootDirectory+"/"+"bin");
                   vector<string> subV;
                   f->getFileList(&subV);
                   for(auto t=subV.begin(); t!=subV.end(); ++t)
                   {
                       string res ="bin/"+*t;
                       v->push_back(res);

                   }

               }
               else
               {
//                   cout << "dir name is: " << dp->d_name << endl;
                   v->push_back(dp->d_name);
               }


           }
        }
        closedir(dirp);
    }


};

#endif //LOGH_ARC_PACKER__FILE_READER_H
