//
// Created by dd on 28.10.2019.
//

#include <iostream>
#include <cstdlib>
#include <string>
#include "parsers/header-parser.h"
#include "parsers/file-parser.h"
#include "parsers/file-extractor.h"

#ifndef LOGH_ARC_PACKER__ARC_PARSER_H
#define LOGH_ARC_PACKER__ARC_PARSER_H

using namespace std;

class ArcParser{

    //(0x1 0x2) -> this means, this is 2 byte data and you cant separate them
    // + -> math sum

    //(0x0  0x3) is archive type
    //0x4 is file meta start address or length of full header (which is almost 128byte(0x80) in everywhere)
    //0xD is file count (matches with file meta count and file name count)
    //(0x12 0x13) + 0x8 gives start address of file name list



public:
    unsigned char *archiveBytes;
    int archiveSize = 0;
    HeaderParser *headerParser;
    FileParser *fileParser;
    FileExtractor *fileExtractor;
    string archiveFileName;
    string outputDirectoryPath;

    void init()
    {

        FILE *f = fopen(this->archiveFileName.c_str(),"rb");
        if(f==NULL)
        {
            printf("CANT OPEN FILE");
            exit(1);
        }

        fseek(f,0L,SEEK_END); //send pointer to end of file
        int fsize= ftell(f); //get position of pointer
        fseek(f,0L,SEEK_SET); //send pointer to beginning of file

        unsigned char * buffer; //alloc memory as file size
        buffer = (unsigned char *)malloc(fsize);
        fread(buffer,fsize,1,f);//read file and fill to buffer
        fclose(f); //close file so other things can read that file too
        printf("size of file: %lu \n", sizeof(buffer));

        this->archiveBytes = buffer;
        this->archiveSize = fsize;
        //printf("size archive is: %lu",sizeof(archive));


        this->fileExtractor = new FileExtractor();
        this->fileExtractor->setExtractDirectory(this->outputDirectoryPath);
        //init
        this->initHeaderParser();
        this->initFileParser();
        //init


        //parse
        //printf("example data : %c \n",this->archiveBytes[0]);
        this->headerParser->setHeaderBytes(this->archiveBytes);

        this->fileParser->setArchiveBytes(this->archiveBytes);
        this->fileParser->setHeaderParser(*this->headerParser);
        //parse


        this->debugLines();
    }

    void setOutputDirectoryPath(string path)
    {
        this->outputDirectoryPath = path;
    }

    void setArchiveFileName(string fileName)
    {
        this->archiveFileName = fileName;
    }

    void extractFiles()
    {
        this->fileExtractor->extractFilesToDirectory(this->fileParser);
    }

    void initHeaderParser()
    {
        this->headerParser = new HeaderParser();
    }

    void initFileParser()
    {
        this->fileParser = new FileParser();
    }

    void debugLines()
    {
        //printf("example data : %c%c%c%c \n", this->headerParser->archiveType[0], this->headerParser->archiveType[1], this->headerParser->archiveType[2], this->headerParser->archiveType[3]);

        //cout << "Archive Type is:" << this->headerParser->archiveType << "\n";
    }


};



#endif //LOGH_ARC_PACKER__ARC_PARSER_H
